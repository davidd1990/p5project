let poblacion;
let order = [];
let neworder = [];
let maletas = [];
let numeroPoblacion = 100;
let numeroObjetos = 20;
let bolsadeObjetos = [];
let maximo = 0;
let contar = 0;

function setup() {
   createCanvas(400, 300);
  for (let i = 0; i < numeroObjetos; i++) {
    bolsadeObjetos.push(new Objeto(i));
  }
   for (let i = 0; i < numeroObjetos; i++) {
    if(bolsadeObjetos[i].gusto > maximo){
      maximo = bolsadeObjetos[i].gusto;
    }
  }
  for (let i = 0; i < numeroObjetos; i++) {
       bolsadeObjetos[i].gusto /= maximo;
       bolsadeObjetos[i].gusto *= 100;
  }
  poblacion = new Poblacion(numeroPoblacion,bolsadeObjetos);
}


function draw() {
  background(50);
  textAlign(CENTER, CENTER);
  poblacion.evaluar();
  poblacion.nextGeneration(bolsadeObjetos);
  for (let y = 0 ; y < poblacion.bestMaleta.grupo.length; y++){
    let posy = ((width /poblacion.bestMaleta.grupo.length)) / 2;
    let xpos = (width /poblacion.bestMaleta.grupo.length) * y ;
    let ypos = y * 100;
      fill(255);
    rect(xpos ,height / 4, width / poblacion.bestMaleta.grupo.length  , 50);
     fill(51);
    text(round(poblacion.bestMaleta.grupo[y].peso,2), xpos + posy,(height / 4) + 25);
    text(round(poblacion.bestMaleta.grupo[y].id,2), xpos + posy,(height / 4) + 35);
     text(round(poblacion.bestMaleta.grupo[y].gusto,2), xpos + posy,(height / 4) + 45);
      stroke(100);
  }
  console.log(poblacion)
  contar++;

  

}
