class Poblacion {
  constructor(numeroPoblacion,bolsaObjetos) {
      this.pobla = [];
     for (let i = 0; i < numeroPoblacion; i++ ){
       let maleta = new Maleta();
       maleta.setGrupo(bolsaObjetos);
       this.pobla.push(maleta);
     }
     this.pool = [];
     this.bestMaleta = {};
     this.maxMaleta = 0;
  }
  getPobla () {  
     return this.pobla;
  } 
  evaluar () {
    let maxfit = 0;
     let maxlength = 0;
     let maxgusto=0;
     let fitnnestamano = 0;
     let fitnessdistancia =  0;
    this.pool = [];
    for (let i = 0; i < this.pobla.length; i++ ){
  
     this.pobla[i].calcularFitnness(); 
      if (this.pobla[i].fitness > maxfit){
         maxfit = this.pobla[i].fitness;
       }
       if (this.pobla[i].grupo.length > maxlength){
         maxlength = this.pobla[i].grupo.length;
       }
       
    }
      for (let i = 0 ; i < this.pobla.length; i ++ ) {
         
        fitnnestamano = map(this.pobla[i].grupo.length, 0,7,0,100);
     
        this.pobla[i].fitness = (this.pobla[i].fitness + fitnnestamano) /2  ;
        if (this.pobla[i].fitness > 80){
          this.pool.push(this.pobla[i]);
        }
        if (this.pobla[i].fitness > this.maxMaleta){
          this.bestMaleta = this.pobla[i];
          this.maxMaleta = this.pobla[i].fitness;
        }
      }

      
  }
  nextGeneration (bolsaObjetos) {
    let newPoblacion = [];

    for (let i = 0 ; i < this.pobla.length; i ++){
       let orderA = this.pickOne(this.pobla);
       let orderB = this.pickOne(this.pobla);
       let arre = this.whoisBiger(orderA,orderB);
       let order = this.crossOver(arre[0], arre[1]);
       this.mutate(order, 0.01,bolsaObjetos);
       newPoblacion[i] = order;
    }
    this.pobla = [];
    this.pobla = newPoblacion;
  }
  whoisBiger (a,b) {
    let arr = [];
    if (a.grupo.length > b.grupo.length) {
      arr[0] = a;
      arr[1] = b;
      return arr;
    }else {
      arr[0] = b;
      arr[1] = a;
      return arr;
    }
  }
  mutate(order,mutationRate,bolsaObjetos) {
    for (let i = 0; i < order.grupo.length; i++) {
      if (random(1) < mutationRate){
        let indexA = floor(random(order.grupo.length));
        let obketo = random(bolsaObjetos);
        if (!order.grupo.includes(obketo)){
           order.grupo[indexA] = obketo;
        }  
      }
    }
  }
  swap(a,i, j) {
     let temp = a[i];
     a[i]= a[j];
     a[j] = temp;
  }
  crossOver(orderA,orderB) {
     let neworder = [];
      let peso = 0;
     let start = floor(random(orderB.grupo.length));
     let end = floor(random(start + 1, orderB.grupo.length));
     neworder = orderB.grupo.slice(start, end);
     for (let y = 0; y < neworder.length; y ++){
         peso += neworder[y].getPeso();
     }
     for (let i = 0; i < orderA.grupo.length; i++ ){
       let maleta = orderA.grupo[i];
       if (peso > orderA.limit ) {
         
       } else {
            if (!neworder.includes(maleta)) {
         neworder.push(maleta);
         peso += orderA.grupo[i].getPeso();
       }
       }
     
    ;
     }
     let male = new Maleta(neworder);
     return male;
  }
  pickOne(list) {
    let index = 0;
    let r = random(1);
    r = r * 100;
    while (r>0){
      r = r - list[index].fitness;
      index ++;
    }
    index--;
    return list[index];
  }
 
}
