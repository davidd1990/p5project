class Maleta {
  constructor (objetos) {
      if (objetos) {
        this.grupo = objetos;
      }else {
       this.grupo = [];
      }
      this.limit = 30;
      this.maxfit2 = 0;
      this.fitness = 0;
      this.maxgusto = 0;
  
  } 
  calcularFitnness () {
    for (let i = 0; i < this.grupo.length; i++) {
      this.maxfit2 += this.grupo[i].getPeso();
    } 
      for (let i = 0; i < this.grupo.length; i++) {
      this.maxgusto += this.grupo[i].gusto;
    }
    if (this.maxfit2 > this.limit) {
        this.fitness = 1;
    } else {
    let d = map(this.maxfit2, 0, 30, 0, 100);
    this.fitness = (d + this.maxgusto) / 2;
    
    }
    
    
  } 
 setGrupo (bolsaObjetos) {
       while (this.maxfit2 <= this.limit) {
           let x = random(bolsaObjetos);
           let suma = this.maxfit2 + x.getPeso();
         
           if (suma > this.limit) {
           
              break;
           }else {
               if (!this.grupo.includes(x)){
            this.grupo.push(x);
            this.maxfit2 += x.getPeso();
           }
           }
          
          
            
          
   }
 }
  
}
